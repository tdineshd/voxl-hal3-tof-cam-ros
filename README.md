# voxl-hal3-tof-cam-ros

Hal3 Tof app that gets RAW TOF data from the camera module. The app then calls the PMD libs to do the post-processing of the RAW data. The post-processed data like IR Image, Depth Image etc is published to ROS topics that can be viewed using the RViz tool.

## Build sample project:

- Clone project:

```bash
git clone https://gitlab.com/voxl-public/voxl-hal3-tof-cam-ros.git
cd voxl-hal3-tof-cam-ros
```

- Run voxl-emulator docker
  - Note: information regarding the voxl-emulator can be found [here](https://gitlab.com/voxl-public/voxl-docker)

```bash
voxl-docker -i voxl-emulator
```

- Build project binary:

```bash
source /opt/ros/indigo/setup.bash
./build.sh source/
```

This will generate a binary in "devel/lib/voxl_hal3_tof_cam_ros/voxl_hal3_tof_cam_ros_node"

- Build the IPK

```bash
./make_package.sh
```

- If VOXL is connected via adb, you can install on  target;

```bash
./install_on_voxl.sh
```

## Visualize

1. (PC) adb shell
1. (VOXL) RUN ifconfig
    * This will give you the IP address of the VOXL
    * For WiFi setup, instructions found [here](https://docs.modalai.com/wifi-setup/)
1. (VOXL) export ROS_IP=IP-ADDR-OF-VOXL
1. (VOXL) source /opt/ros/indigo/setup.bash
1. (VOXL) roscore &
    * This will print a line that should read something like this: "ROS_MASTER_URI=http://AAA.BBB.CCC.DDD:XYZUV/"
    * The http address will be different
1. (PC) ifconfig
    * This will give you the IP address of the PC
1. (PC) export ROS_IP=IP-ADDR-OF-PC
1. (PC) export ROS_MASTER_URI=http://AAA.BBB.CCC.DDD:XYZUV/
    * The ROS_MASTER_URI is what you saw on the VOXL when you ran "roscore &" in the above step
1. (PC) source /opt/ros/xxxxx/setup.bash
    * xxxxxx is the ROS version you have installed
1. (PC) The 2 transforms (for both laser scan and point cloud) are in the tof.launch file
    * In RViz choose Fixed Frame toption as /base_link instead of map
    * Open your .rviz file (by default in /home/xxx/.rviz/default.rviz)
    * vi /home/xxx/.rviz/default.rviz
    * Change the "Fixed Frame:" option from "map" to "base_link"
        Global Options:
            Fixed Frame: /base_link
1. (PC) rviz
    * On the leftmost column click on the "Add" button
    * In the pop-up options click on "Image"
    * Change Display Name to "IR-Image"
    * In the left column under "IR-Image" tab select type in Image Topic as /voxl_ir_image_raw
    * Click on the "Add" button again
    * In the pop-up options click on "Image"
    * Change Display Name to "Depth-Image"
    * In the left column under "Depth-Image" tab select type in Image Topic as /voxl_depth_image_raw
    * "Add" PointCloud2 with Topic as "/voxl_point_cloud"
    * "Add" LaserScan with Topic as "/voxl_laser_scan"
1. (VOXL) Modify the "camera_id" field in tof.launch to select the correct camera id for your setup
    * By default it is 1
    * File is in this directory: /opt/ros/indigo/share/voxl_hal3_tof_cam_ros/launch/tof.launch
    * For camera-id please check [here](https://docs.modalai.com/camera-connections/)
1. (VOXL) roslaunch /opt/ros/indigo/share/voxl_hal3_tof_cam_ros/launch/tof.launch
    * By default all outputs are enabled i.e. IR-Image, Depth-Image, Point-Cloud






