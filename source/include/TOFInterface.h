/*******************************************************************************
 * Copyright 2020 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#ifndef __TOF_INTERFACE_H
#define __TOF_INTERFACE_H

#include <stdint.h>

namespace tof
{

// -----------------------------------------------------------------------------------------------------------------------------
// These are the post processed forms of TOF camera data
// -----------------------------------------------------------------------------------------------------------------------------
typedef enum
{
    LISTENER_NONE               = 0x0,
    LISTENER_DEPTH_DATA         = 0x1,
    LISTENER_SPARSE_POINT_CLOUD = 0x2,
    LISTENER_DEPTH_IMAGE        = 0x4,
    LISTENER_IR_IMAGE           = 0x8
} RoyaleListenerType;

static const uint32_t MaxRoyaleListenerTypes = 4;

// -----------------------------------------------------------------------------------------------------------------------------
// This is the listener client that the TOF bridge library will call when it has post processed data from the Royale PMD libs
// -----------------------------------------------------------------------------------------------------------------------------
class IRoyaleDataListener
{
public:
    IRoyaleDataListener() {};
    virtual ~IRoyaleDataListener() {};
    virtual bool RoyaleDataDone(const void* pData, uint32_t size, int64_t timestamp, RoyaleListenerType dataType) = 0;
};

}

// -----------------------------------------------------------------------------------------------------------------------------
// TOF Initialization data
// -----------------------------------------------------------------------------------------------------------------------------
struct TOFInitializationData
{
    void*                     pTOFInterface;        ///< TOF Interface pointer
    uint32_t                  numDataTypes;         ///< Type of listener types
    tof::RoyaleListenerType*  pDataTypes;           ///< tof::RoyaleListenerType
    tof::IRoyaleDataListener* pListener;            ///< Class object of type tof::IRoyaleDataListener
    uint8_t                   frameRate;            ///< TOF camera Frame rate
};

// -----------------------------------------------------------------------------------------------------------------------------
// These are the main interfaces to the TOF Bridge library
// -----------------------------------------------------------------------------------------------------------------------------
extern "C" {
void* TOFCreateInterface();
int   TOFInitialize(TOFInitializationData* pTOFInitializationData);
void  TOFProcessRAW16(void* pTOFInterface, uint16_t* pRaw16PixelData, uint64_t timestamp);
void  TOFDestroyInterface(void* pTOFInterface);
}

#endif // __TOF_INTERFACE_H