/*******************************************************************************
 * Copyright 2020 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include <errno.h>
#include <getopt.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "hello_hal3_camera.h"
#include "TOFInterface.h"

// Global to this file CameraHAL3 pointer
static CameraHAL3* g_pCameraHAL3 = NULL;

// -----------------------------------------------------------------------------------------------------------------------------
// Main Exit
// -----------------------------------------------------------------------------------------------------------------------------
void Hal3MainExit()
{
    if (g_pCameraHAL3 != NULL)
    {
        g_pCameraHAL3->Stop();
        delete g_pCameraHAL3;
        g_pCameraHAL3 = NULL;
    }
}

// -----------------------------------------------------------------------------------------------------------------------------
// Main Enter
// -----------------------------------------------------------------------------------------------------------------------------
int Hal3MainEnter(int argc, char* const argv[], ros::NodeHandle rosNodeHandle)
{
    int         status      = 0;
    // All the default values can be overwritten with the command line args. If the command line args is not specified, the
    // defaults are used
    int         cameraid          = 0;
    int         width             = 224;
    int         height            = 1557;
    int         format            = PreviewFormatBLOB;
    int         mode              = CameraModePreview;
    int         tofdatatype       = 0; // 1 << tof::RoyaleListenerType::LISTENER_IR_IMAGE;
    int         tofnumframes      = 0;
    int         dumpPreviewFrames = 0;
    bool        publishDepthImage;
    bool        publishIRImage;
    bool        publishPointCloud;
    bool        publishLaserScan;

    rosNodeHandle.param<bool>("publish_depth_image", publishDepthImage, false);
    rosNodeHandle.param<bool>("publish_ir_image", publishIRImage, false);
    rosNodeHandle.param<bool>("publish_point_cloud", publishPointCloud, false);
    rosNodeHandle.param<bool>("publish_laser_scan", publishLaserScan, false);

    if (publishDepthImage == true)
    {
        printf("\n\tDEPTH_IMAGE");
        tofdatatype |= tof::RoyaleListenerType::LISTENER_DEPTH_IMAGE;
    }

    if (publishIRImage || publishPointCloud || publishLaserScan)
    {
        printf("\n\tIR: %d ..... Cloud: %d ..... Laser: %d", publishIRImage, publishPointCloud, publishLaserScan);
        tofdatatype |= tof::RoyaleListenerType::LISTENER_DEPTH_DATA;
    }

    if (status == 0)
    {
        g_pCameraHAL3 = new CameraHAL3;

        printf("\nCamera id: %d", cameraid);
        printf("\nImage width: %d", width);
        printf("\nImage height: %d", height);
        printf("\nNumber of frames to dump: %d", dumpPreviewFrames);
        printf("\nCamera mode: preview");

        status = g_pCameraHAL3->Initialize();
    }

    if (status == 0)
    {
        // Making this function call will start the camera (cameraid) and it will start streaming frames
        status = g_pCameraHAL3->Start(cameraid,
                                      width,
                                      height,
                                      (PreviewFormat)format,
                                      (CameraMode)mode,
                                      tofdatatype,
                                      tofnumframes,
                                      NULL,
                                      dumpPreviewFrames,
                                      rosNodeHandle);
    }

    printf("\n\n");
    return status;
}
