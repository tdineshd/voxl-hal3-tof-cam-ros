/*******************************************************************************
 * Copyright 2020 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include <stdio.h>
#include "hello_hal3_camera.h"

// -----------------------------------------------------------------------------------------------------------------------------
// Constructor
// -----------------------------------------------------------------------------------------------------------------------------
CameraHAL3::CameraHAL3()
{
    m_moduleCallbacks = {CameraDeviceStatusChange, TorchModeStatusChange};

    for (uint32_t i = 0; i < MaxCameras; i++)
    {
        m_pPerCameraMgr[i] = NULL;
    }
}

// -----------------------------------------------------------------------------------------------------------------------------
// Performs any one time initialization. This function should only be called once.
// -----------------------------------------------------------------------------------------------------------------------------
int CameraHAL3::Initialize()
{
    int status = hw_get_module(CAMERA_HARDWARE_MODULE_ID, (const hw_module_t**)&m_pCameraModule);

    if (status == 0)
    {
        printf("\nSUCCESS: Camera module opened");
    }
    else
    {
        printf("\nERROR: Cannot open camera module!");
    }

    if (status == 0)
    {
        if (m_pCameraModule->init != NULL)
        {
            status = m_pCameraModule->init();
        }
    }

    if (status == 0)
    {
        m_numCameras = m_pCameraModule->get_number_of_cameras();

        for (int i = 0 ;i < m_numCameras;i++)
        {
            // This gives the camera's fixed characteristics that can be extracted from the camera_metadata
            // "info.static_camera_characteristics"
            status = m_pCameraModule->get_camera_info(i, &m_cameraInfo[i]);

            if (status == 0)
            {
                printf("\nCamera Id: %d Facing: %d", i, m_cameraInfo[i].facing);
            }
            else
            {
                fprintf(stderr, "\nError getting info for camera: %d", i);
                break;;
            }
        }
    }

    if (status == 0)
    {
        status = m_pCameraModule->set_callbacks(&m_moduleCallbacks);
    }

    return status;
}

// -----------------------------------------------------------------------------------------------------------------------------
// This function opens the camera and starts sending the capture requests
// -----------------------------------------------------------------------------------------------------------------------------
int CameraHAL3::Start(int             cameraid,           ///< Camera id to open
                      int             width,              ///< Image buffer width
                      int             height,             ///< Image buffer height
                      PreviewFormat   format,             ///< Image buffer format
                      CameraMode      mode,               ///< Preview / Video
                      int             tofdatatype,        ///< Tof data type
                      int             tofnumframes,       ///< Number of Tof frames to dump
                      const char*     pVideoFilename,     ///< Video filename for Video mode
                      int             dumpPreviewFrames,  ///< Number of preview frames to dump
                      ros::NodeHandle rosNodeHandle)      ///< ROS node handle
{
    int status = -EINVAL;

    if (cameraid < m_numCameras)
    {
        m_pPerCameraMgr[cameraid] = new PerCameraMgr(rosNodeHandle);

        // Initialize the per camera manager
        status = m_pPerCameraMgr[cameraid]->Initialize(m_pCameraModule,
                                                       &m_cameraInfo[cameraid],
                                                       cameraid,
                                                       width,
                                                       height,
                                                       format,
                                                       mode,
                                                       tofdatatype,
                                                       tofnumframes,
                                                       pVideoFilename,
                                                       dumpPreviewFrames);

        if (status == 0)
        {
            // Start the camera which will start sending requests and processing results from the camera module
            status = m_pPerCameraMgr[cameraid]->Start();
        }
    }
    else
    {
        printf("\nInvalid camera id %d. There are only %d cameras", cameraid, m_numCameras);
    }
    
    return status;
}

// -----------------------------------------------------------------------------------------------------------------------------
// This function informs the per camera manager to stop the camera and stop sending any more requests
// -----------------------------------------------------------------------------------------------------------------------------
void CameraHAL3::Stop()
{
    for (uint32_t i = 0; i < MaxCameras; i++)
    {
        if (m_pPerCameraMgr[i] != NULL)
        {
            m_pPerCameraMgr[i]->Stop();
            m_pPerCameraMgr[i] = NULL;
        }
    }
}